from models import Checkout, pricing_rule
import db, json, locale

if __name__ == "__main__":
    data = None
    with open("data.json") as data_file:
        data = json.load(data_file)
    db.init(data)

    basket = None
    with open("basket.json") as basket_file:
        basket = json.load(basket_file)

    co = Checkout(pricing_rule.listAll())
    for product_code in basket:
        co.scan(product_code)

    price = co.total()
    
    locale.setlocale(locale.LC_ALL, 'en_IE.UTF-8' )
    print "Items: " + ", ".join(basket)
    print "Total: %s" % locale.currency(price/100)
