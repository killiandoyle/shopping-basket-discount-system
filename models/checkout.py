import product

class Checkout:
    """ Checkout object which uses pricing rules
        to calculate discounts
    """
    def __init__(self, pricing_rules):
        self.items = []
        self.pricing_rules = pricing_rules or []
    
    def scan(self, code):
        self.items.append(product.get(code))

    def total(self):
        amount = 0
        all_prices = [item.price for item in self.items]
        discounts = []
        for rule in self.pricing_rules:
            indexes = [x[0] for x in enumerate(self.items) if rule.match(x[1])]
            prices = (all_prices[i] for i in indexes)
            discounted = rule.calc((p for p in prices))
            for i, d in zip(indexes, discounted): all_prices[i] = d
        return sum(tuple(all_prices))
