import db

class Product:
    def __init__(self, code, name, price, currency):
        self.code = code
        self.name = name
        self.price = price
        self.currency = currency

class ProductNotFoundException(Exception): pass

def get(code):
    table = db.tables['products']
    for row in table:
        if row[0] == code: return Product(*row)
    raise ProductNotFoundException()
