class Rule:
    def match(self, item):
        """
            Returns:
                bool: True if item matches the rule
        """
        raise NotImplementedError()
