import db, logging, math
from rule import Rule

class PricingRule(Rule):
    """ Pricing Rule interface / class in which
        a rule is calculated if a condition is satisfied
    """

    def __init__(self, condition):
        """ Args:
                condition (str): is true when it's equal to the product code
        """
        self.condition = condition

    def match(self, product):
        if type(self.condition) == str or type(self.condition) == unicode:
            return self.condition == product.code
        #Can implement list of items as well as regex similarly

    def calc(self, prices):
        """ Args:
                prices list|tuple(int): a list of prices for each item in the 
                    basket which satisfies the condition
        """
        raise NotImplementedError()

class BulkPricingRule(PricingRule):
    """ Pricing rule for a bulk purchase in which all the items 
        are discounted if the quantity exceeds a threshold. 
        e.g. A 30% discount if 5 or more items are purchased.
    """
    def __init__(self, condition, threshold, discount_rate):
        """ Args:
                threshold (int): the number of items which must be exceeded
                     for discount to be applied
                discount_rate (int): the percentage rate at which items
                    are to be discounted
        """
        PricingRule.__init__(self, condition)
        assert(discount >= 0 and discount <= 100)
        self.threshold = threshold
        self.discount_rate = discount_rate
        
    def __str__(self):
        return "Bulk %s %s " % (self.threshold, self.discount_rate)

    def calc(self, prices):
        prices = tuple(prices)
        if len(prices) < self.threshold: return prices
        return tuple((p * (1 - self.discount_rate / 100.) for p in prices))

class BOGOPricingRule(PricingRule):
    """ Buy One Get One Discounted Pricing rule 
        OR 
        Buy X And Get Y Discounted by Z%.
    """
    def __init__(self, condition, quantity, n_disconum_discountedunted, discount_rate):
        """ Args:
                quantity (int): The number of items you need to get the discount
                num_discounted (int): The number of items to be discoutned
                discount_rate (int): The percentage rate at which items are discounted
        """
        PricingRule.__init__(self, condition)
        assert(discount >= 0 and discount <= 100)
        self.quantity = quantity
        self.num_discounted = num_discounted
        self.discount = discount
    
    def __str__(self):
        return "BOGOD  %s %s %s " % (self.quantity, self.num_discounted, self.discount) 

    def calc(self, prices):
        prices = tuple(prices)
        discounted = list(prices)
        for i in range(0, int(math.floor(len(prices) / self.quantity))):
            for j in range(0, self.n_discounted):
                index = (i + 1) * self.quantity - j - 1
                if index < len(prices): discounted[index] = prices[i + j] * (1 - self.discount / 100.)
        return tuple(discounted)

class BOGOFPricingRule(BOGOPricingRule):
    """ The Buy One Get One Free or Buy X Get Y Free """

    def __init__(self, condition, quantity, n_discounted):
        BOGOPricingRule.__init__(self, condition, quantity, n_discounted, 100)


classMap = {
    'BULK': BulkPricingRule,
    'BOGOF': BOGOFPricingRule
}

def listAll():
    rules = []
    for row in db.tables["pricing_rules"]:
        if row[0] not in classMap:
            logging.warning("Invalid rule list " + str(row))
        else:
            clss = classMap[row[0]]
            rules.append(clss(*row[1:]))
    return rules
