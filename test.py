import unittest, db
from models import BOGOPricingRule, BulkPricingRule, BOGOFPricingRule, Checkout

class PricingTestCase(unittest.TestCase):
    def test_bogo(self):
        cases = [
            [BOGOFPricingRule("VOUCHER", 3, 1), (5,) * 2, (5, 5)],
            [BOGOFPricingRule("VOUCHER", 3, 1), (5,) * 3, (5, 5, 0)],
            [BOGOFPricingRule("VOUCHER", 3, 1), (5,) * 4, (5, 5, 0, 5)],
            [BOGOFPricingRule("VOUCHER", 3, 1), (5,) * 5, (5, 5, 0, 5, 5)],
            [BOGOFPricingRule("VOUCHER", 3, 1), (5,) * 6, (5, 5, 0, 5, 5, 0)],

            [BOGOFPricingRule("VOUCHER", 3, 2), (5,) * 2, (5, 5)],
            [BOGOFPricingRule("VOUCHER", 3, 2), (5,) * 3, (5, 0, 0)],
            [BOGOFPricingRule("VOUCHER", 3, 2), (5,) * 4, (5, 0, 0, 5)],
        ]
        for case in cases:
            rule = case[0]; args = case[1]; expected = case[2]
            actual = rule.calc(args)
            self.assertEqual(actual, expected, (str(rule) + " %s -> %s != %s") % (args, actual, expected))

    def test_bulk(self):
        cases = [
            [BulkPricingRule("TSHIRT", 3, 5), (20,) * 1, (20,)],
            [BulkPricingRule("TSHIRT", 3, 5), (20,) * 2, (20, 20)],
            [BulkPricingRule("TSHIRT", 3, 5), (20,) * 3, (19, 19, 19)]
        ]
        for case in cases:
            rule = case[0]; args = case[1]; expected = case[2]
            actual = rule.calc(args)
            self.assertEqual(actual, expected, (str(rule) + " %s -> %s != %s") % (args, actual, expected))

class CheckoutTestCase(unittest.TestCase):
    def setUp(self):
        data = {
            'products': [
                ("VOUCHER", "NoviCap Voucher", 500, "eur"),
                ("TSHIRT", "NoviCap T-Shirt", 2000, "eur"),
                ("MUG", "NoviCap Coffee Mug", 750, "eur")
            ],
            'pricing_rules': [
                ("BOGO", "VOUCHER", 2, 1),
                ("BULK", "TSHIRT", 3, 5)
            ]
        }
        db.init(data)

    def test_multi(self):

        cases = [
            [(BOGOFPricingRule("VOUCHER", 2, 1), BulkPricingRule("TSHIRT", 3, 5)), [
                [("VOUCHER", "TSHIRT", "MUG"), 3250],
                [("VOUCHER", "TSHIRT", "VOUCHER"), 2500],
                [("TSHIRT", "TSHIRT", "TSHIRT", "VOUCHER", "TSHIRT"), 8100],
                [("VOUCHER", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "TSHIRT", "TSHIRT"), 7450]]]
        ]
        for case in cases:
            rules = case[0]; 
            for c in case[1]:
                codes = c[0]; expected = c[1]
                co = Checkout(rules)
                for code in codes:
                    co.scan(code)
                actual = co.total()
                self.assertEqual(actual, expected)
